'use strict';

(function () {
    angular
        .module('Main', [
            'ui.router',
            'ui.bootstrap',
            'ngAnimate',
            'satellizer',
            'Users'
        ])
        .run(runMainModule);

    runMainModule.$inject = ['$auth', '$http'];
    function runMainModule($auth, $http) {
        if ($auth.isAuthenticated()) {
            $http.defaults.headers.common.Authorization = "Bearer " + $auth.getToken();
        }
    }
})();