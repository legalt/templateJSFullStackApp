(function () {
    'use strict';

    angular
        .module('Main')
        .config(config);

    config.$inject = ['$authProvider', '$locationProvider', '$compileProvider'];
    function config($authProvider, $locationProvider, $compileProvider) {
        $locationProvider.html5Mode({enabled: true, requireBase: false});
        $authProvider.loginRedirect = 0;
        $authProvider.httpInterceptor = false;
        $authProvider.tokenName = 'token';
        $authProvider.tokenPrefix = 'access';
    }
})();