/**
 * Created by legal on 26.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Main')
        .controller('mainController', mainController);

    mainController.$inject = ['$auth', '$http', '$rootScope'];
    function mainController($auth, $http, $rootScope) {
        var self = this;

        self.isAuthenticated = $auth.isAuthenticated;

        if ($auth.isAuthenticated()) {
            $http.defaults.headers.common.Authorization = "Bearer " + $auth.getToken();
        }

        $rootScope.$watch(function () {
            return $http.pendingRequests;
        }, function (count) {
            if (count <= 1) {
                console.log(count);
                onChangedState();
            }
        });

        function onChangedState() {
            var _overlay = document.getElementsByClassName('overlay')[0];
            var _bodyClass = document.body.className;

            if (_overlay) {
                _overlay.remove();
                document.body.className = _bodyClass.replace('in-load', '');
            }
        }
    }
})();