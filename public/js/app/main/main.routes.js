'use strict';

(function () {
    angular
        .module('Main')
        .config(routes);

    routes.$inject = ['$stateProvider'];
    function routes($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                controller: 'homeController',
                controllerAs: 'home',
                templateUrl: '/partials/main/home.html'
            })
    }
})();