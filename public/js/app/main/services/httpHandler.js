/**
 * Created by legal on 26.07.2016.
 */
(function () {
    angular
        .module('Main')
        .factory('httpHandler', httpHandler);

    httpHandler.$inject = ['$q'];
    function httpHandler($q) {
        function susses(response) {
            return $q.resolve(response.data);
        }

        function error(response) {
            return $q.reject(response.data.message);
        }

        return {
            success: susses,
            error: error
        };
    }
})();