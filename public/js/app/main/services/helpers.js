/**
 * Created by legal on 27.07.2016.
 */
(function () {
    angular
        .module('Main')
        .factory('helpers', helpers);

    function helpers() {
        return {
            dataURItoBlob: dataURItoBlob,
            clearInputFile: clearInputFile
        };

        function clearInputFile(f) {
            if (f.value) {
                try {
                    f.value = ''; //for IE11, latest Chrome/Firefox/Opera...
                } catch (err) {
                }
                if (f.value) { //for IE5 ~ IE10
                    var form = document.createElement('form'), ref = f.nextSibling;
                    form.appendChild(f);
                    form.reset();
                    ref.parentNode.insertBefore(f, ref);
                }
            }
        }

        function dataURItoBlob(dataURI) {
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0) {
                byteString = atob(dataURI.split(',')[1]);
            } else {
                byteString = unescape(dataURI.split(',')[1]);
            }
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {type: mimeString});
        }
    }
})();