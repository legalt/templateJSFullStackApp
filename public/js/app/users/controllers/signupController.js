/**
 * Created by legal on 26.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Users')
        .controller('signUpController', signUpController);

    signUpController.$inject = ['$http', '$auth', '$state', 'authorizationService'];
    function signUpController($http, $auth, $state, authorizationService) {
        var self = this;

        self.user = {};
        self.errors = {};
        self.signUp = signUp;

        function signUp() {
            authorizationService.signUp(self.user).then(function onSuccess(response) {
                $auth.setToken(response.token);
                $http.defaults.headers.common.Authorization = "Bearer " + response.token;
                $state.go('home');
            }, function onError(response) {
                self.errors = response;
            });
        }
    }
})();