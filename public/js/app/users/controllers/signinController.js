/**
 * Created by legal on 26.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Users')
        .controller('signInController', signInController);

    signInController.$inject = ['$http', '$auth', '$state', 'authorizationService'];
    function signInController($http, $auth, $state, authorizationService) {
        var self = this;

        self.user = {};
        self.errors = {};
        self.signIn = signIn;

        function signIn() {
            authorizationService.signIn(self.user).then(function onSuccess(response) {
                $auth.setToken(response.token);
                $http.defaults.headers.common.Authorization = "Bearer " + response.token;
                $state.go('home');
            }, function onError(response) {
                console.log(response);
                self.errors = response;
            });
        }
    }
})();