/**
 * Created by legal on 26.07.2016.
 */
(function () {
    angular
        .module('Users')
        .controller('logoutController', logoutController);

    logoutController.$inject = ['$auth', '$http', '$state'];
    function logoutController($auth, $http, $state) {
        $auth.removeToken();
        delete $http.defaults.headers.common.Authorization;
        $state.go('home');
    }
})();