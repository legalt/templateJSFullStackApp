/**
 * Created by legal on 27.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Users')
        .controller('profileController', profileController);

    profileController.$inject = ['$timeout', 'profileService', 'helpers'];
    function profileController($timeout, profileService, helpers) {
        var self = this;
        var profilePhoto = {};

        self.user = {};
        /* TODO: need service with directive for alerts (exm: messageCenterService)*/
        self.messages = {
            errors: null,
            success: null
        };

        self.update = update;
        self.fileUpload = fileUpload;

        (function activate() {
            profileService.get().then(function (response) {
                self.user = response;
            });
        })();

        function update() {
            var _model = angular.copy(self.user);

            if (profilePhoto.avatar) {
                _model.avatar = profilePhoto.avatar;
            }

            profileService.update(_model).then(function () {
                self.messages.success = 'Profile has been successfully updated';
                $timeout(function () {
                    self.messages.success = null;
                }, 3000);
            }, function (resonse) {
                self.messages.errors = resonse.message;
                $timeout(function () {
                    self.messages.success = null;
                }, 3000);
            });
        }

        function fileUpload(element, model) {
            self.messages = {
                errors: null,
                success: null
            };

            var photofile = element.files[0];
            helpers.clearInputFile(element);
            var reader = new FileReader();

            if (photofile.type.search('image') === -1) {
                self.messages.errors = 'Only png and jpg image formats are allowed';
                $timeout(function () {
                    self.messages.errors = null;
                }, 3000);
            } else {
                if ((photofile.type.search('png') !== -1 || photofile.type.search('jpeg') !== -1)) {
                    reader.onload = function (e) {
                        if (photofile.size > 2072576) {
                            self.messages.errors = 'The size of the image is greater than 2MB';
                            $timeout(function () {
                                self.messages.errors = null;
                            }, 3000);
                        } else {
                            $timeout(function () {
                                self.user[model] = e.target.result;
                                profilePhoto[model] = helpers.dataURItoBlob(e.target.result);
                            });
                        }
                    };
                } else {
                    self.messages.errors = 'Only png and jpg image formats are allowed';
                    $timeout(function () {
                        self.messages.errors = null;
                    }, 3000);
                }
            }
            reader.readAsDataURL(photofile);
        }
    }
})();
