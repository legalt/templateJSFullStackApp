/**
 * Created by legal on 27.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Users')
        .factory('profileService', profileService);

    profileService.$inject = ['$http', 'httpHandler'];
    function profileService($http, httpHandler) {
        function getProfile() {
            var request = $http.get('/api/v1/profile');
            return (request.then(httpHandler.success, httpHandler.error));
        }

        function updateProfile(userData) {
            var form = new FormData();
            form = Object.toFormData(userData);
            
            if (typeof userData.avatar === 'object') {
                form.append('avatar', userData.avatar.file, userData.avatar.name);
            }



            var request = $http({
                method: 'put',
                url:  '/api/v1/profile',
                data: form,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            });

            return (request.then(httpHandler.success, httpHandler.error));
        }

        return {
            get: getProfile,
            update: updateProfile
        };
    }
})();