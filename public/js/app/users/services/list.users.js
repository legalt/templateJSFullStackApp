/**
 * Created by legal on 27.07.2016.
 */
(function () {
    angular
        .module('Users')
        .factory('listUsers', listUsers);

    listUsers.$inject = ['$http', 'httpHandler'];
    function listUsers($http, httpHandler) {
        function getList() {
            var request = $http.get('/api/v1/users');
            return (request.then(httpHandler.success, httpHandler.error));
        }

        return getList;
    }
})();