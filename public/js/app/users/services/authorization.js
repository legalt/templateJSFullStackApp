/**
 * Created by legal on 26.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Users')
        .factory('authorizationService', authorizationService);

    authorizationService.$inject = ['$http', 'httpHandler'];
    function authorizationService($http, httpHandler) {
        return {
            signIn: function (userData) {
                var request = $http.post('/api/v1/signin', userData);
                return (request.then(httpHandler.success, httpHandler.error));
            },
            signUp: function (userData) {
                var request = $http.post('/api/v1/signup', userData);
                return (request.then(httpHandler.success, httpHandler.error));
            }
        };
    }
})();