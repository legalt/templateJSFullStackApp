'use strict';

(function () {
    angular
        .module('Users')
        .config(routes);

    routes.$inject = ['$stateProvider'];
    function routes($stateProvider) {
        $stateProvider
            .state('signin', {
                url: '/singin',
                controller: 'signInController',
                controllerAs: 'form',
                templateUrl: '/partials/users/signin.html'
            })
            .state('signup', {
                url: '/singup',
                controller: 'signUpController',
                controllerAs: 'form',
                templateUrl: '/partials/users/signup.html'
            })
            .state('logout', {
                url: '/logout',
                controller: 'logoutController',
                controllerAs: 'logout',
                templateUrl: '/partials/users/signup.html'
            })
            .state('profile', {
                url: '/profile',
                controller: 'profileController',
                controllerAs: 'profile',
                templateUrl: '/partials/users/profile.html'
            })
    }
})();