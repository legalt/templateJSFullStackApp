/**
 * Created by legal on 28.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Shipping')
        .directive('formRequest', formRequest);

    function formRequest(){
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/partials/shipping/form-request.html',
            link: postLink
        };

        function postLink(scope, element, attr) {
            
        }
    }
})();