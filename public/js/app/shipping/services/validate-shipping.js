/**
 * Created by legal on 28.07.2016.
 */
(function () {
    'use strict';

    angular
        .module('Shipping')
        .factory('validateShipping', validateShipping);

    function validateShipping() {
        return validate;

        function validate(model) {
            var _errors = [];
            var _emailValidatePattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
            var attributes = {
                'nameRequired': 'Name is required',
                'phoneRequired': 'Phone is required',
                'emailRequired': 'Email is required',
                'emailIncorrect': 'Incorrect email',
                'organization': ''
            };
        }
    }
})();