/**
 * Created by legal on 26.07.2016.
 */
require.config({
    paths: {
        "app": "/js/app.min"
    },
    shim: {}
});


require(['app'], runApplication);


function runApplication() {
    angular.element().ready(function () {
        angular.bootstrap(document.documentElement, ['Main'], {strictDi: true});
    });
}
