var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var multer = require('multer');
var config = require('./api/config');
var crypto = require('crypto');
var mime = require('mime');
var fs = require('fs');

process.env.ROOT_DIR = __dirname;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        try {
            fs.lstatSync(config.UPLOAD_PATH).isDirectory();
        }
        catch (e) {
            fs.mkdirSync(config.UPLOAD_PATH);
        }
        cb(null, config.UPLOAD_PATH)
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
        });
    }
});

process.CONST = config;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded(({extended: true})));
app.use(multer({storage: storage}).any());
app.use('/', express.static(__dirname + '/public'));
app.use(config.UPLOAD_PUBLIC_PATH, express.static(__dirname + '/uploads'));

require('./api/routes/routes')(app, express);

app.listen(config.EXPRESS_HTTP_PORT, function () {
    console.log('Server listen port: ' + config.EXPRESS_HTTP_PORT);
});