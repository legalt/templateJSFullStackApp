module.exports = routes;


function routes(app, express) {
    var middleWares = require('./routesMiddleware');
    var router = express.Router();

    router.use('/api/v1/signin', middleWares.accessGuestUser);
    router.use('/api/v1/signup', middleWares.accessGuestUser);
    router.use('/api/v1/profile', middleWares.accessAuthorizedUser);

    router
        .post('/api/v1/signin', require(process.env.ROOT_DIR + '/api/controllers/users/authorization/signInController'))
        .post('/api/v1/signup', require(process.env.ROOT_DIR + '/api/controllers/users/authorization/signUpController'))
        .get('/api/v1/profile', require(process.env.ROOT_DIR + '/api/controllers/users/profile/ReadProfileController'))
        .put('/api/v1/profile', require(process.env.ROOT_DIR + '/api/controllers/users/profile/UpdateProfileController'))
        .get('/api/v1/users', require(process.env.ROOT_DIR + '/api/controllers/users/listUsersController'))
        .get('*', require(process.env.ROOT_DIR + '/api/controllers/spaController'));


    app.use('/', router);
    app.use(middleWares.main);
}