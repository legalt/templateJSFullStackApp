/**
 * Created by legal on 26.07.2016.
 */
var jwt = require('jsonwebtoken');

function main(request, response) {
    if (request.busboy){
        response.send('aldlasmdlasd');
    }
}

/**
 * MiddleWare for users with role is client
 * @param request
 * @param response
 * @param next
 */
function accessOnlyClient(request, response, next) {
    var token = request.headers['authorization'];

    isValidUser(token, process.CONST.ROLES.CLIENT, request, response, next);
}

/**
 * MiddleWare for users with role is admin
 * @param request
 * @param response
 * @param next
 */
function accessOnlyAdmin(request, response, next) {
    var token = request.headers['authorization'];

    isValidUser(token, process.CONST.ROLES.ADMIN, request, response, next);
}

/**
 * MiddleWare for users just is authorized
 * @param request
 * @param response
 * @param next
 */
function accessAuthorizedUser(request, response, next) {
    var token = request.headers['authorization'];

    isValidUser(token, -1, request, response, next);
}

/**
 * MiddleWare for users just is guest
 * @param request
 * @param response
 * @param next
 * @returns {*|{encode, decode, is, equals, pattern}|{type, shorthand}}
 */
function accessGuestUser(request, response, next) {
    var token = request.headers['authorization'];
    if (!token) {
        next();
    } else {
        return response.status(process.CONST.HTTP_CODES.FORBIDDEN)
            .json({
                message: 'Only guest'
            });
    }
}

function isValidUser(token, needRole, request, response, next) {
    if (!token) {
        return response.status(process.CONST.HTTP_CODES.UNAUTHORIZED)
            .json({message: 'Unauthorized.'});
    }

    token = token.replace('Bearer', '').trim();

    jwt.verify(token, process.CONST.JWT_SECRET, function onResult(err, result) {
        if (err) {
            return response.status(process.CONST.HTTP_CODES.UNAUTHORIZED)
                .json({message: 'Unauthorized.'});
        }

        if (needRole >= 0 && result.role !== needRole) {
            return response.status(process.CONST.HTTP_CODES.FORBIDDEN)
                .json({message: 'Forbidden'});
        } else {
            request.decoded = result;
            return next();
        }
    });
}

module.exports = {
    main: main,
    accessOnlyClient: accessOnlyClient,
    accessOnlyAdmin: accessOnlyAdmin,
    accessAuthorizedUser: accessAuthorizedUser,
    accessGuestUser: accessGuestUser
};