/**
 * Created by legal on 26.07.2016.
 */
var mongoose = require('mongoose');
mongoose.connect(process.CONST.DATABASE_URL);

var emailValidatePattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
var imageTypeRegularExpression = /\/(.*?)$/;
var crypto = require('crypto');
var fs = require('fs');
var helpers = require(process.env.ROOT_DIR + '/api/helpers');
var Schema = mongoose.Schema;
var model;

var UserSchema = new Schema({
    email: {
        type: String,
        required: [true, 'Email is required'],
        validate: {
            validator: function (email) {
                return emailValidatePattern.test(email);
            },
            message: '{VALUE} is not a valid email!'
        }
    },
    name: {type: String, default: ''},
    organization: {type: String, default: ''},
    phone: {type: String, default: ''},
    avatar: {
        type: String,
        default: ''
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
        minlength: 5,
        maxlength: 15
    },
    role: Number
});

UserSchema
    .pre('save', function (next, done) {
        var self = this;

        self.findOne({email: self.email}, function (err, user) {
            if (err) {
                done(err);
            } else if (user) {
                self.invalidate("email", "email must be unique");
                done(new Error("email must be unique"));
            } else {
                self.password = crypto.createHash('md5').update(self.password).digest('hex');
                next();
            }
        });
    })
    .pre('update', function (next, done) {
        var self = this;
        
        self.findOne({id: self._id}, function (err, user) {
            if (user.avatar !== self._update.$set.avatar) {
                var _avatar = user.avatar;
                var _fileNameRegExp = new RegExp(/[\w-]+\.[\w]+/);
                var _rootDir = process.env.ROOT_DIR + process.CONST.UPLOAD_PATH + '/';

                _avatar = _avatar.match(_fileNameRegExp);
                _rootDir = _rootDir.replace('.', '');

                fs.exists(_rootDir + _avatar, function (result) {
                    if (!result) {
                        next();
                    } else {
                        fs.unlink(_rootDir + _avatar, function () {
                            next();
                        });
                    }
                });
            } else {
                next();
            }
        });
    });

model = mongoose.model('Users', UserSchema);

module.exports = model;