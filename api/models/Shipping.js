/**
 * Created by legal on 28.07.2016.
 */
var mongoose = require('mongoose');
mongoose.connect(process.CONST.DATABASE_URL);
var Schema = mongoose.Schema;
var model;
var emailValidatePattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

var ShippingSchema = new Schema({
    name: {type: String, require: [true, 'Name is required']},
    phone: {type: String, require: [true, 'Phone is required']},
    email: {
        type: String, require: [true, 'Email is required'], required: [true, 'Email is required'],
        validate: {
            validator: function (email) {
                return emailValidatePattern.test(email);
            },
            message: '{VALUE} is not a valid email!'
        }
    },
    organization: {type: String, require: [true, 'Organization is required']},
    payment: {type: Number, require: [true, 'Payment is required']},
    pointA: {type: String, require: [true, 'Point of departure is required']},
    pointB: {type: String, require: [true, 'Point of destination is required']},
    deadlineDate: {type: String, require: [true, 'Shipment date is required']},
    status: {type: Number, require: true},
    userId: String
});

model = mongoose.model('Shipping', ShippingSchema);

module.exports = model;