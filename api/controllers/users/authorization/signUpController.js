/**
 * Created by legal on 26.07.2016.
 */

function signUpController(request, response) {
    var User = require(process.env.ROOT_DIR + '/api/models/Users');
    var jwt = require('jsonwebtoken');   
    var user = new User();

    user.email = request.body.email;
    user.password = request.body.password;
    user.role = process.CONST.ROLES.CLIENT;

    user.save(function (err) {
        if (err) {
            return response.status(400).json({
                message: err.toString()
            });
        }

        var token = jwt.sign({
            email: request.body.email,
            role: user.role,
            _id: user._id
        }, process.CONST.JWT_SECRET);

        return response.status(200).json({
            token: token,
            email: user.email,
            role: user.role,
            _id: user._id
        });
    });
}

module.exports = signUpController;
