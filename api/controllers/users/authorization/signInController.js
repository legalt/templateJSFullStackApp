/**
 * Created by legal on 26.07.2016.
 */

function signInController(request, response, next) {
    var User = require(process.env.ROOT_DIR + '/api/models/Users');
    var jwt = require('jsonwebtoken');
    var crypto = require('crypto');

    if (!request.body.email) {
        return response.status(400).json({
            message: 'Email is required'
        });
    } else if (!request.body.password) {
        return response.status(400).json({
            message: 'Password is required'
        });
    }

    var _hashedPassword = crypto.createHash('md5').update(request.body.password).digest('hex');

    User.findOne({email: request.body.email}, function (err, user) {
        if (!user) {
            return response.status(400).json({
                message: 'User not found with email: ' + request.body.email
            });
        } else {
            if (_hashedPassword !== user.password) {
                return response.status(400).json({
                    message: 'Incorrent password'
                });
            }

            var token = jwt.sign({
                email: request.body.email,
                role: user.role,
                _id: user._id
            }, process.CONST.JWT_SECRET);

            return response.status(200).json({
                token: token,
                email: user.email,
                role: user.role,
                _id: user._id
            });
        }
    });

}

module.exports = signInController;
