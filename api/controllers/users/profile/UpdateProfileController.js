/**
 * Created by legal on 27.07.2016.
 */

function updateProfileController(request, response) {
    var Users = require(process.env.ROOT_DIR + '/api/models/Users');
    var _ = require('lodash');

    if (request.files) {
        _.forEach(request.files, function (file) {
            request.body[file.fieldname] = process.CONST.UPLOAD_PUBLIC_PATH + '/' + file.filename;
        });         
        update();
    } else {
        update();
    }

    function update() {
        Users.update({_id: request.decoded._id}, {
            name: request.body.name,
            email: request.body.email,
            avatar: request.body.avatar,
            organization: request.body.organization,
            phone: request.body.phone
        }, function (err, user) {
            if (err) {
                return response.status(process.CONST.HTTP_CODES.BAD_REQUEST)
                    .json({
                        message: err.toString()
                    });
            }

            return response.status(process.CONST.HTTP_CODES.SUCCESS)
                .json({
                    success: true
                });
        });
    }
}

module.exports = updateProfileController;

