/**
 * Created by legal on 26.07.2016.
 */
var fs = require('fs');

function profileController(request, response) {
    var token = request.headers['authorization'];
    var Users = require(process.env.ROOT_DIR + '/api/models/Users');
    
    Users.findOne({_id: request.decoded._id}, 'name _id avatar email', function (err, user) {
        if (err){
            return response.status(process.CONST.HTTP_CODES.INTERNAL_ERROR)
                .json({
                    message: err.toString()
                });
        }

        return response.status(process.CONST.HTTP_CODES.SUCCESS)
            .json(user);
    });


}

module.exports = profileController;