/**
 * Created by legal on 27.07.2016.
 */

function listController(request, response) {
    var Users = require(process.env.ROOT_DIR + '/api/models/Users');

    Users.find({}, 'name avatar _id email', function (err, users) {
        if (err) {
            return response.status(process.CONST.HTTP_CODES.INTERNAL_ERROR)
                .json({
                    message: err.toString()
                });
        }

        return response.status(process.CONST.HTTP_CODES.SUCCESS)
            .json({
                list: users
            });
    });
}

module.exports = listController;