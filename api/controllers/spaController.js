/**
 * Created by legal on 26.07.2016.
 */

function spaController(request, response) {
    var path = require('path');
    
    response.sendFile(path.join(process.env.ROOT_DIR, 'public', 'index.html'));

}

module.exports = spaController;