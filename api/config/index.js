/**
 * Created by legal on 26.07.2016.
 */
module.exports = {
    'JWT_SECRET': 'aklsdmaskldmakldmlasd',
    'DATABASE_URL': 'mongodb://localhost/test',
    'EXPRESS_HTTP_PORT': 8080,
    'ROLES': {
        'ADMIN': 0,
        'CLIENT': 1
    },
    'HTTP_CODES': {
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        SUCCESS: 200,
        INTERNAL_ERROR: 500
    },
    UPLOAD_PATH: './uploads',
    UPLOAD_PUBLIC_PATH: '/uploads'
};