'use strict';

module.exports = function (grunt) {
    var config = {};

    //region loaded tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-bower-task');
    //endregion

    //region Init tasks
    config = require('./grunt/config')(grunt);

    grunt.initConfig(config);
    //endregion

    //region Register tasks
    grunt.registerTask('prebuild', ['bower:spa']);
    grunt.registerTask('buildSPA', ['concat:spa', 'uglify:spa', 'clean:spa']);
    //endregion
};