/**
 * Created by legal on 26.07.2016.
 */
'use strict';

function generateModuleList() {
    var glob = require('glob'),
        csdf = [],
        modules = glob.sync('public/js/app/**/*.module.js'),
        list = [
            'public/js/libs/object-traverse.js',
            'public/js/libs/object-to-form-data.js',
            'public/js/libs/angular/angular.js',
            'public/js/libs/angular-animate/angular-animate.js',
            'public/js/libs/angular-bootstrap/ui-bootstrap-tpls.js',
            'public/js/libs/angular-ui-router/angular-ui-router.js',
            'public/js/libs/satellizer/satellizer.js'
        ],
        i,
        files = glob.sync('public/js/app/**/*.js');

    if (Array.isArray(modules)) {
        for (i = 0; i < modules.length; i += 1) {
            if (modules[i].indexOf('main.module.js') === -1 && modules[i].indexOf('module.js') >= 0) {
                if (list.indexOf(modules[i]) === -1) {
                    list.push(modules[i]);
                }
            }
        }
    }

    if (Array.isArray(files)) {
        for (i = 0; i < files.length; i += 1) {
            if (files[i].indexOf('module.js') === -1 && files[i].indexOf('app/main') === -1) {
                if (csdf.indexOf(files[i]) === -1) {
                    csdf.push(files[i]);
                }
            }
        }
    }
    list = list.concat(csdf);

    list.push('public/js/app/main/main.module.js');
    list.push('public/js/app/main/main.routes.js');
    list.push('public/js/app/main/main.config.js');
    list.push('public/js/app/main/controllers/**/*.js');
    list.push('public/js/app/main/directives/**/*.js');
    list.push('public/js/app/main/filters/**/*.js');
    list.push('public/js/app/main/services/**/*.js');

    return list;
}

module.exports = {
    generateModuleList: generateModuleList
};