/**
 * Created by legal on 26.07.2016.
 */
module.exports = {
    PATH_TO_LIBS:       './public/js/libs',
    PATH_TEMP_SPA_APP:  './public/js/temp',
    PATH_SPA_APP:       './public/js',

    TEMP_NAME_APP_FILE: 'app.js',
    NAME_APP_FILE:      'app.min.js'
};