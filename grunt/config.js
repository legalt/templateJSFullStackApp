/**
 * Created by legal on 26.07.2016.
 */
module.exports = function (grunt) {
    return {
        pkg: grunt.file.readJSON('package.json'),
        concat: require('./tasks/concat'),
        uglify: require('./tasks/uglify'),
        clean: require('./tasks/clean'),
        cssmin: require('./tasks/cssmin'),
        bower: require('./tasks/bower')
    };
};