/**
 * Created by legal on 26.07.2016.
 */
var Const = require('./../constants');

module.exports = {
    spa:{
        options: {
            copy: true,
            targetDir: Const.PATH_TO_LIBS,
            layout: 'byType',
            install: true,
            cleanBowerDir: true,
            bowerOptions: {}
        }
    }
};