/**
 * Created by legal on 26.07.2016.
 */
var _ = require('lodash');
var Const = require('./../constants');

module.exports = {
    options: {
        compress: {
            drop_console: true
        }
    },
    spa: {
        src: Const.PATH_TEMP_SPA_APP + '/' + Const.TEMP_NAME_APP_FILE,
        dest: Const.PATH_SPA_APP + '/' + Const.NAME_APP_FILE
    },
    api: {}
};