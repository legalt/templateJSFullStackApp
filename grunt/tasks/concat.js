/**
 * Created by legal on 26.07.2016.
 */
'use strict';

var Functions = require('./../functions');
var Const = require('./../constants');

module.exports = {
    spa: {
        src: Functions.generateModuleList(),
        dest: Const.PATH_TEMP_SPA_APP + '/' + Const.TEMP_NAME_APP_FILE
    },
    api: {}
};